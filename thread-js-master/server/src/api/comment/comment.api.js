import { CommentsApiPath, PostsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.REACTS, (req, res, next) => commentService
      .getReactions(req.query)
      .then(response => {
        return res.send(response);
      })
      .catch(next))
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.ROOT, (req, res, next) => commentService
      .update(req.body)
      .then(comment => {
        return res.send(comment);
      })
      .catch(next))
    .put(CommentsApiPath.REACT, (req, res, next) => commentService
      .setReaction(req.user.id, req.body)
      .then(reaction => {
        if (reaction.comment && reaction.comment.userId !== req.user.id) {
          // notify a user if someone (not himself) liked his post
          req.io
            .to(reaction.comment.userId)
            .emit('commentLike', reaction.dataValues);
        }
        return res.send(reaction);
      })
      .catch(next))
    .delete(CommentsApiPath.$ID, (req, res, next) => commentService
      .deleteCommentById(req.params.id)
      .then(result => {
        return res.status(200).json({
          message: `Comment id: ${req.params.id} deleted`
        });
      })
      .catch(next));

  return router;
};

export { initComment };
