class Comment {
  constructor({ commentRepository, commentReactionRepository }) {
    this._commentRepository = commentRepository;
    this._commentReactionRepository = commentReactionRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  update(data) {
    const {id, body} = data;
    return this._commentRepository.updateById(id, {body});
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  getReactions(filter) {
    return this._commentReactionRepository.getCommentReactions(filter);
  }

  async setReaction(userId, { commentId, isLike = true }) {
    const updateOrDelete = (react) => (react.isLike === isLike
      ? this._commentReactionRepository.deleteById(react.id)
      : this._commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._commentReactionRepository.getCommentReaction(
      userId,
      commentId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._commentReactionRepository.create({ userId, commentId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._commentReactionRepository.getCommentReaction(userId, commentId);
  }

  deleteCommentById(commentId) {
    return this._commentRepository.deleteById(commentId);
  }
}

export { Comment };
