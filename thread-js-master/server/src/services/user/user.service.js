class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  update(data) {
    const {id, username, status, imageId} = data;
    return this._userRepository.updateById(id, {username, status, imageId});
  }
}

export { User };
