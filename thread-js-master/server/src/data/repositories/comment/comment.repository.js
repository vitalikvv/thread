import { Abstract } from '../abstract/abstract.repository';
import { sequelize } from '../../db/connection';

class Comment extends Abstract {
  constructor({ commentModel, userModel, imageModel, commentReactionModel }) {
    super(commentModel);
    this._userModel = userModel;
    this._imageModel = imageModel;
    this._commentReactionModel = commentReactionModel;
  }

  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id',
        'commentReactions.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`(
                        SELECT COUNT("commentReactions"."commentId")
                        FROM "commentReactions"
                        WHERE "commentId" = '${id}'
                        AND "isLike" = true
                        )`), 'commentLikesCount'],
          [sequelize.literal(`(
                        SELECT COUNT("commentReactions"."commentId")
                        FROM "commentReactions"
                        WHERE "commentId" = '${id}'
                        AND "isLike" = false
                        )`), 'commentDislikesCount'],
        ]
      },
      include: [
        {
          model: this._userModel,
          attributes: ['id', 'username', 'status'],
          include: {
            model: this._imageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: this._commentReactionModel,
          attributes: ['isLike', 'userId'],
        }
      ]
    });
  }
}

export { Comment };
