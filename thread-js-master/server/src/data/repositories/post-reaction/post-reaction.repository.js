import { Abstract } from '../abstract/abstract.repository';

class PostReaction extends Abstract {
  constructor({ postReactionModel, postModel, userModel }) {
    super(postReactionModel);
    this._postModel = postModel;
    this._userModel = userModel;
  }

  getPostReactions(filter) {
    const {postId, listOfLikesOrDis} = filter;
    let where = {};
    if (listOfLikesOrDis === 'like') {
      Object.assign(where, { postId, isLike: true });
    }
    if (listOfLikesOrDis === 'dislike') {
      Object.assign(where, { postId, isLike: false });
    }
    return this.model.findAll({
      group: [
        'postReaction.id',
        'user.id'
      ],
      attributes: ['isLike', 'postId'],
      where,
      include: [
        {
          model: this._userModel,
          attributes: ['username', 'id']
        }
      ]
    })
  }

  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: ['postReaction.id', 'post.id', 'user.id'],
      where: { userId, postId },
      include: [
        {
          model: this._postModel,
          attributes: ['id', 'userId']
        }, {
          model: this._userModel,
          attributes: ['username']
        }
      ]
    });
  }

}

export { PostReaction };
