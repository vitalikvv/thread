import { Abstract } from '../abstract/abstract.repository';

class CommentReaction extends Abstract {
  constructor({ commentReactionModel, commentModel, userModel }) {
    super(commentReactionModel);
    this._commentModel = commentModel;
    this._userModel = userModel
  }

  getCommentReactions(filter) {
    const {commentId, listOfLikesOrDis} = filter;
    let where = {};
    if (listOfLikesOrDis === 'like') {
      Object.assign(where, { commentId, isLike: true });
    }
    if (listOfLikesOrDis === 'dislike') {
      Object.assign(where, { commentId, isLike: false });
    }
    return this.model.findAll({
      group: [
        'commentReaction.id',
        'user.id'
      ],
      attributes: ['isLike', 'commentId'],
      where,
      include: [
        {
          model: this._userModel,
          attributes: ['username', 'id']
        }
      ]
    })
  }

  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: ['commentReaction.id', 'comment.id', 'user.id'],
      where: { userId, commentId },
      include: [
        {
          model: this._commentModel,
          attributes: ['id', 'userId']
        }, {
          model: this._userModel,
          attributes: ['username']
        }
      ]
    });
  }
}

export { CommentReaction };
