import { HttpMethod, ContentType } from 'src/common/enums/enums';

class Comment {
  constructor({ http }) {
    this._http = http;
  }

  getAllLikes(filter) {
    return this._http.load('/api/comments/reacts', {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.GET
    });
  }

  addComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  updateComment(payload) {
    return this._http.load('/api/comments', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likeComment(commentId, likeOrDislike) {
    return this._http.load('/api/comments/react', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        commentId,
        isLike: likeOrDislike === 'like'
      })
    });
  }

  deleteComment(id) {
    return this._http.load(`/api/comments/${id}`, {
      method: HttpMethod.DELETE
    });
  }
}

export { Comment };
