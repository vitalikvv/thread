import { createAction } from '@reduxjs/toolkit';
import { StorageKey } from 'src/common/enums/enums';
import {
  storage as storageService,
  auth as authService,
  image as imageService
} from 'src/services/services';

const ActionType = {
  SET_USER: 'profile/set-user'
};

const setUser = createAction(ActionType.SET_USER, user => ({
  payload: {
    user
  }
}));

const login = request => async dispatch => {
  const { user, token } = await authService.login(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const register = request => async dispatch => {
  const { user, token } = await authService.registration(request);

  storageService.setItem(StorageKey.TOKEN, token);
  dispatch(setUser(user));
};

const logout = () => dispatch => {
  storageService.removeItem(StorageKey.TOKEN);
  dispatch(setUser(null));
};

const loadCurrentUser = () => async dispatch => {
  const user = await authService.getCurrentUser();

  dispatch(setUser(user));
};

const changeUserData = request => async dispatch => {
  try {
    await authService.updateUserData(request);
    const user = await authService.getCurrentUser();
    dispatch(setUser(user));
  } catch (err) {
    console.log(err.message);
  }
};

const changeUserPhoto = request => async dispatch => {
  const { userID, data } = request;

  const image = await imageService.updateImage(data);

  await authService.updateUserData({ imageId: image.id, id: userID });
  const user = await authService.getCurrentUser();
  dispatch(setUser(user));
};

export {
  setUser,
  login,
  register,
  logout,
  loadCurrentUser,
  changeUserData,
  changeUserPhoto
};
