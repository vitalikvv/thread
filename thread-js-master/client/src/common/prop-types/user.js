import PropTypes from 'prop-types';

const userType = PropTypes.exact({
  id: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  image: PropTypes.object,
  imageId: PropTypes.string,
  username: PropTypes.string.isRequired,
  status: PropTypes.string,
  createdAt: PropTypes.string.isRequired,
  updatedAt: PropTypes.string.isRequired
});

export { userType };
