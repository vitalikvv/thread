import * as React from 'react';
import PropTypes from 'prop-types';
import { Grid, Icon, Input, NavLink } from 'src/components/common/common';
import { useDispatch } from 'react-redux';
import * as profileActionCreator from '../../../../store/profile/actions';

import styles from './styles.module.scss';
import { AppRoute } from '../../../../common/enums/app/app-route.enum';

const TopUserDescription = ({ userName, userStatus, userID }) => {
  const [status, setStatus] = React.useState(userStatus);
  const [isUpdateMode, setIsUpdateMode] = React.useState(false);
  const dispatch = useDispatch();

  const handleUserStatusUpdate = value => {
    setStatus(value);
  };

  const toEditMode = () => {
    setIsUpdateMode(true);
  };

  const handleChangeUserData = payload => {
    dispatch(profileActionCreator.changeUserData(payload));
    setIsUpdateMode(false);
  };

  return (
    <Grid.Column rows={2}>
      <NavLink exact to={AppRoute.ROOT} style={{ color: 'black' }}>
        <Grid.Row>
          {userName}
        </Grid.Row>
      </NavLink>
      {!isUpdateMode && (
        <Grid.Row className={styles.status} onClick={toEditMode}>
          {status}
          &ensp;
          <Icon disabled name="pencil" />
        </Grid.Row>
      )}
      {isUpdateMode && (
        <Input
          placeholder="Type status"
          type="text"
          transparent
          autoFocus
          className={styles.data_input}
          onChange={event => handleUserStatusUpdate(event.target.value)}
          onBlur={() => handleChangeUserData({ id: userID, status })}
          value={status}
        />
      )}
    </Grid.Column>
  );
};
TopUserDescription.defaultProps = {
  userStatus: undefined
};

TopUserDescription.propTypes = {
  userName: PropTypes.string.isRequired,
  userID: PropTypes.string.isRequired,
  userStatus: PropTypes.string
};

export default TopUserDescription;
