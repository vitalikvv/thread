import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Dropdown, Menu, Popup, Loader } from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({
  post,
  authorizedUser,
  onPostLike,
  onExpandedPostToggle,
  sharePost,
  onEditOwnPost,
  onDeleteOwnPost,
  onDisplayList }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    userId
  } = post;
  const date = getFromNowTime(createdAt);
  const handlePostLike = likeOrDislike => onPostLike(id, likeOrDislike);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditOwnPost = () => onEditOwnPost(post);
  const handleDeleteOwnPost = () => onDeleteOwnPost(post.id);

  const likesListDisplay = listOfLikesOrDis => onDisplayList(id, listOfLikesOrDis);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra className={styles.cardFooter}>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => handlePostLike('like')}
        >
          <Icon name={IconName.THUMBS_UP} />
        </Label>
        <span className={styles.popupWrapper}>
          {likeCount > 0 && (
            <Popup
              className={styles.popup}
              on="hover"
              inverted
              position="bottom right"
              size="mini"
              trigger={(
                <span
                  className={styles.digitWrapper}
                  onMouseEnter={() => likesListDisplay('like')}
                >
                  {likeCount}
                </span>
              )}
            >
              {
                post.reactionListOfLikes
                  ? post.reactionListOfLikes.userList.map(item => <div key={item.id}>{item.username}</div>)
                  : <Loader active inline inverted />
              }
            </Popup>
          )}
        </span>

        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => handlePostLike('dislike')}
        >
          <Icon name={IconName.THUMBS_DOWN} />
        </Label>
        <span className={styles.popupWrapper}>
          {dislikeCount > 0 && (
            <Popup
              className={styles.popup}
              on="hover"
              inverted
              position="bottom right"
              size="mini"
              trigger={(
                <span
                  className={styles.digitWrapper}
                  onMouseEnter={() => likesListDisplay('dislike')}
                >
                  {dislikeCount}
                </span>
              )}
            >
              {
                post.reactionListOfDislikes
                  ? post.reactionListOfDislikes.userList.map(item => <div key={item.id}>{item.username}</div>)
                  : <Loader active inline inverted />
              }
            </Popup>
          )}
        </span>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
        </Label>
        <span
          className={styles.digitWrapper}
          style={{ cursor: 'pointer', marginRight: '7px' }}
          onClick={handleExpandedPostToggle}
        >
          {commentCount}
        </span>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {authorizedUser === userId && (
          <Menu
            borderless
            style={{ position: 'absolute', top: 0, right: 0, marginRight: 14, border: 'none', boxShadow: 'none' }}
          >
            <Dropdown
              item
              text="Actions"
            >
              <Dropdown.Menu>
                <Dropdown.Item onClick={handleEditOwnPost}>Update post</Dropdown.Item>
                <Dropdown.Item onClick={handleDeleteOwnPost}>Delete post</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Menu>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  authorizedUser: PropTypes.string.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onEditOwnPost: PropTypes.func.isRequired,
  onDeleteOwnPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  onDisplayList: PropTypes.func.isRequired
};

export default Post;
