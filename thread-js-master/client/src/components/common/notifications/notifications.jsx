import * as React from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import {
  NotificationContainer,
  NotificationManager
} from 'react-notifications';
import { ENV } from 'src/common/enums/enums';
import { userType } from 'src/common/prop-types/prop-types';

import 'react-notifications/lib/notifications.css';

const socket = io(ENV.SOCKET_URL);

const Notifications = ({ user, onPostApply, onChangeLikesCount, onUpdateExpandedPost }) => {
  socket.open();
  React.useEffect(() => {
    if (!user) {
      return undefined;
    }
    const { id } = user;
    socket.emit('createRoom', id);
    socket.on('like', answer => {
      if (answer.isLike) {
        NotificationManager.success(`Your post was liked by user ${answer.user.username}`);
      } else {
        NotificationManager.warning(`Your post was disliked by user ${answer.user.username}`);
      }
      onChangeLikesCount(answer.post.id);
    });
    socket.on('commentLike', answer => {
      if (answer.isLike) {
        NotificationManager.success(`Your comment was liked by user ${answer.user.username}`);
      } else {
        NotificationManager.warning(`Your comment was disliked by user ${answer.user.username}`);
      }
      onUpdateExpandedPost(answer.comment.id);
    });
    socket.on('new_post', post => {
      if (post.userId !== id) {
        NotificationManager.info('New post was applied!');
        onPostApply(post.id);
      }
    });

    return () => {
      socket.open();
    };
  }, []);
  return <NotificationContainer />;
};

Notifications.defaultProps = {
  user: undefined
};

Notifications.propTypes = {
  user: userType,
  onPostApply: PropTypes.func.isRequired,
  onChangeLikesCount: PropTypes.func.isRequired,
  onUpdateExpandedPost: PropTypes.func.isRequired
};

export default Notifications;
