import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input } from 'src/components/common/common';
import * as profileActionCreator from '../../../store/profile/actions';

const UserDescription = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const [userName, setUserName] = React.useState(user.username);
  const [status, setStatus] = React.useState(user.status ?? '');
  const dispatch = useDispatch();

  const handleUserNameUpdate = value => {
    setUserName(value);
  };

  const handleUserStatusUpdate = value => {
    setStatus(value);
  };

  const handleChangeUserData = payload => {
    dispatch(profileActionCreator.changeUserData(payload));
  };

  return (
    <>
      <br />
      <Input
        icon="user"
        iconPosition="left"
        placeholder="Username"
        type="text"
        onChange={event => handleUserNameUpdate(event.target.value)}
        onBlur={() => handleChangeUserData({ username: userName, id: user.id, status })}
        value={userName}
      />
      <br />
      <br />
      <Input
        icon="at"
        iconPosition="left"
        placeholder="Email"
        type="email"
        disabled
        value={user.email}
      />
      <br />
      <br />
      <Input
        icon="universal access"
        iconPosition="left"
        placeholder="Your status"
        type="text"
        onChange={event => handleUserStatusUpdate(event.target.value)}
        onBlur={() => handleChangeUserData({ username: userName, id: user.id, status })}
        value={status}
      />
    </>
  );
};

export default UserDescription;
