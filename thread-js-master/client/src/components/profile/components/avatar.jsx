import * as React from 'react';
import { useCallback, useEffect, useRef, useState } from 'react';
import ReactCrop from 'react-image-crop';
import { Image } from 'src/components/common/common';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import PropTypes from 'prop-types';
import { imageType } from 'src/common/prop-types/prop-types';
import Button from '../../common/button/button';
import styles from './avatar.module.scss';
import 'react-image-crop/dist/ReactCrop.css';
import { IconName } from '../../../common/enums/components/icon-name.enum';

const Avatar = ({ handleChangeUserPhoto, userId, userAvatar }) => {
  const [upImg, setUpImg] = useState();
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  const [crop, setCrop] = useState({
    unit: 'px',
    aspect: 1
  });
  const [completedCrop, setCompletedCrop] = useState(null);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  function handleUploadFile(canvas, finalCrop) {
    if (!finalCrop || !canvas) {
      return;
    }

    canvas.toBlob(
      data => {
        handleChangeUserPhoto({ data, userID: userId });
        setUpImg(false);
      },
      'image/png',
      1
    );
  }

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const cropping = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = cropping.width * pixelRatio;
    canvas.height = cropping.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      cropping.x * scaleX,
      cropping.y * scaleY,
      cropping.width * scaleX,
      cropping.height * scaleY,
      0,
      0,
      cropping.width,
      cropping.height
    );
  }, [completedCrop]);
  return (
    <>
      {!upImg && (
        <div>
          <p style={{ fontWeight: 700 }}>
            Click to modify photo:
          </p>
          <Button className={styles.userAvatar}>
            <label>
              <Image
                centered
                src={userAvatar?.link ?? DEFAULT_USER_AVATAR}
                size="medium"
                circular
              />
              <input
                name="image"
                type="file"
                onChange={onSelectFile}
                hidden
              />
            </label>
          </Button>
        </div>
      )}
      {upImg && (
        <div>
          <p style={{ fontWeight: 700 }}>
            Crop the photo (photo should be square and maximum 200x200px):
          </p>
          <ReactCrop
            src={upImg}
            onImageLoaded={onLoad}
            crop={crop}
            maxHeight={200}
            maxWidth={200}
            style={{ width: 300 }}
            onChange={c => setCrop(c)}
            onComplete={c => setCompletedCrop(c)}
          />
          <div>
            <p style={{ fontWeight: 700 }}>
              Preview :
            </p>
            <canvas
              ref={previewCanvasRef}
              // Rounding is important so the canvas width and height matches/is a multiple for sharpness.
              style={{
                width: Math.round(completedCrop?.width ?? 0),
                height: Math.round(completedCrop?.height ?? 0)
              }}
            />
          </div>
          <Button
            color="teal"
            iconName={IconName.IMAGE}
            isDisabled={!completedCrop?.width || !completedCrop?.height}
            onClick={() => handleUploadFile(previewCanvasRef.current, completedCrop)}
          >
            Load cropped image
          </Button>
        </div>
      )}
    </>
  );
};
Avatar.defaultProps = {
  userAvatar: undefined
};

Avatar.propTypes = {
  handleChangeUserPhoto: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  userAvatar: imageType
};

export default React.memo(Avatar);
