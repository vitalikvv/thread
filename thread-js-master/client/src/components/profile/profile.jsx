import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid } from 'src/components/common/common';
import * as profileActionCreator from '../../store/profile/actions';
import Avatar from './components/avatar';
import UserDescription from './components/userDescription';

const Profile = () => {
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const dispatch = useDispatch();

  const handleChangeUserPhoto = React.useCallback(payload => {
    dispatch(profileActionCreator.changeUserPhoto(payload));
  }, [dispatch]);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Avatar handleChangeUserPhoto={handleChangeUserPhoto} userId={user.id} userAvatar={user.image} />
        <UserDescription />
      </Grid.Column>
    </Grid>
  );
};

export default Profile;
