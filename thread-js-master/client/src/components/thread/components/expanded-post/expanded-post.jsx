import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';
import UpdatePost from '../update-post/update-post';
import { image as imageService } from '../../../../services/services';
import UpdateComment from '../update-comment/update-comment';

const ExpandedPost = ({
  sharePost
}) => {
  const dispatch = useDispatch();
  const { post, userId } = useSelector(state => ({
    post: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [showUpdateOwnPostComponent, setShowUpdateOwnPostComponent] = React.useState(undefined);
  const [showUpdateOwnCommentComponent, setShowUpdateOwnCommentComponent] = React.useState(undefined);
  const [openModal, setOpenModal] = React.useState(true);

  const handlePostLike = React.useCallback((id, likeOrDislike) => (
    dispatch(threadActionCreator.likePost(id, likeOrDislike))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const onDisplayList = React.useCallback((id, listOfLikesOrDis) => (
    dispatch(threadActionCreator.displayListOfLikes(id, listOfLikesOrDis))
  ), [dispatch]);

  const onDisplayCommentLikesList = React.useCallback((id, listOfLikesOrDis) => (
    dispatch(threadActionCreator.displayCommentListOfLikes(id, listOfLikesOrDis))
  ), [dispatch]);

  const handleEditOwnPost = data => {
    setShowUpdateOwnPostComponent(data);
  };

  const handleEditOwnComment = data => {
    setShowUpdateOwnCommentComponent(data);
  };

  const handleDeletePost = postId => {
    dispatch(threadActionCreator.deletePost(postId));
    handleExpandedPostToggle();
  };

  const handleDeleteComment = commentId => {
    dispatch(threadActionCreator.deleteComment(commentId));
  };

  const handleCommentLike = React.useCallback((commentId, likeOrDislike) => (
    dispatch(threadActionCreator.likeComment(commentId, likeOrDislike))
  ), [dispatch]);

  const sortedComments = getSortedComments(post.comments ?? []);
  const updateImage = file => imageService.updateImage(file);

  React.useEffect(() => {
    if (!openModal) {
      handleExpandedPostToggle();
    }
  }, [openModal]);
  return (
    <Modal
      centered={false}
      open={openModal}
      onClose={() => setOpenModal(false)}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            authorizedUser={userId}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onEditOwnPost={handleEditOwnPost}
            onDeleteOwnPost={handleDeletePost}
            sharePost={sharePost}
            onDisplayList={onDisplayList}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                authorizedUser={userId}
                onEditOwnComment={handleEditOwnComment}
                onCommentLike={handleCommentLike}
                onDeleteOwnComment={handleDeleteComment}
                onDisplayCommentLikesList={onDisplayCommentLikesList}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
      {showUpdateOwnPostComponent && (
        <UpdatePost
          post={showUpdateOwnPostComponent}
          uploadImage={updateImage}
          close={() => setShowUpdateOwnPostComponent(false)}
        />
      )}
      {showUpdateOwnCommentComponent && (
        <UpdateComment
          comment={showUpdateOwnCommentComponent}
          close={() => setShowUpdateOwnCommentComponent(false)}
        />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
