import * as React from 'react';
import PropTypes from 'prop-types';
import { threadActionCreator } from 'src/store/actions';
import { useDispatch } from 'react-redux';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment } from 'src/components/common/common';

import styles from './styles.module.scss';
import { Modal } from '../../../common/common';
import { postType } from '../../../../common/prop-types/post';

const UpdatePost = ({ post, uploadImage, close }) => {
  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image);
  const [isUploading, setIsUploading] = React.useState(false);
  const [openModal, setOpenModal] = React.useState(true);
  const dispatch = useDispatch();

  const handleAddPost = () => {
    if (!body) {
      return;
    }
    dispatch(threadActionCreator.updatePost({ imageId: image?.imageId, body, id: post.id }));
    setBody('');
    setImage(undefined);
    close();
  };

  const handleUploadFile = event => {
    if (event) {
      const { target } = event;
      setIsUploading(true);
      const [file] = target.files;

      uploadImage(file)
        .then(({ id: imageId, link: imageLink }) => {
          setImage({ imageId, imageLink });
        })
        .catch(() => {
          //        dispatch(addTodoFailure(err.message));
          // todo add errorHandler
        })
        .finally(() => {
          setIsUploading(false);
        });
    }
  };

  React.useEffect(() => {
    if (!openModal) {
      close();
    }
  }, [openModal]);

  return (
    <Modal
      open={openModal}
      onClose={() => setOpenModal(false)}
    >
      <Modal.Header className={styles.header}>
        <span>Update your post</span>
      </Modal.Header>
      <Modal.Content>
        <Segment>
          <Form onSubmit={handleAddPost}>
            {(image?.imageLink || image?.link) && (
              <div className={styles.imageWrapper}>
                <Image className={styles.image} src={image?.imageLink || image?.link} alt="post" />
              </div>
            )}
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => setBody(ev.target.value)}
            />
            <div className={styles.btnWrapper}>
              <Button
                color="teal"
                isLoading={isUploading}
                isDisabled={isUploading}
                iconName={IconName.IMAGE}
              >
                <label className={styles.btnImgLabel}>
                  Attach image
                  <input
                    name="image"
                    type="file"
                    onChange={handleUploadFile}
                    hidden
                  />
                </label>
              </Button>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Update
              </Button>
            </div>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

UpdatePost.propTypes = {
  post: postType.isRequired,
  uploadImage: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatePost;
