import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import { Dropdown, Loader, Menu, Popup } from '../../../common/common';
import { IconName } from '../../../../common/enums/components/icon-name.enum';

const Comment = ({
  comment,
  authorizedUser,
  onEditOwnComment,
  onDeleteOwnComment,
  onCommentLike,
  onDisplayCommentLikesList
}) => {
  const {
    body,
    createdAt,
    user,
    id,
    commentLikesCount,
    commentDislikesCount
  } = comment;
  const handleEditOwnComment = () => onEditOwnComment(comment);
  const handleCommentLike = likeOrDislike => onCommentLike(id, likeOrDislike);
  const handleDeleteOwnComment = () => onDeleteOwnComment(id);

  const commentLikesListDisplay = listOfLikesOrDis => onDisplayCommentLikesList(id, listOfLikesOrDis);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        <CommentUI.Text className={styles.status}>{user.status}</CommentUI.Text>
        <CommentUI.Text>{body}</CommentUI.Text>
        <CommentUI.Actions style={{ alignItems: 'center', display: 'flex' }}>
          <CommentUI.Action>
            <span
              className={styles.toolbarBtn}
              onClick={() => handleCommentLike('like')}
            >
              <Icon name={IconName.THUMBS_DOWN} />
            </span>
          </CommentUI.Action>
          <span className={styles.popupWrapper}>
            {commentLikesCount > 0 && (
              <Popup
                className={styles.popup}
                on="hover"
                inverted
                position="bottom right"
                size="mini"
                trigger={(
                  <span
                    className={styles.digitWrapper}
                    onMouseEnter={() => commentLikesListDisplay('like')}
                  >
                    {commentLikesCount}
                  </span>
                )}
              >
                {
                  comment.reactionListOfLikes
                    ? comment.reactionListOfLikes.userList.map(item => <div key={item.id}>{item.username}</div>)
                    : <Loader active inline inverted />
                }
              </Popup>
            )}
          </span>
          <CommentUI.Action>
            <span
              className={styles.toolbarBtn}
              onClick={() => handleCommentLike('dislike')}
            >
              <Icon name={IconName.THUMBS_DOWN} />
            </span>
          </CommentUI.Action>
          <span className={styles.popupWrapper}>
            {commentDislikesCount > 0 && (
              <Popup
                className={styles.popup}
                on="hover"
                inverted
                position="bottom right"
                size="mini"
                trigger={(
                  <span
                    className={styles.digitWrapper}
                    onMouseEnter={() => commentLikesListDisplay('dislike')}
                  >
                    {commentDislikesCount}
                  </span>
                )}
              >
                {
                  comment.reactionListOfDislikes
                    ? comment.reactionListOfDislikes.userList.map(item => <div key={item.id}>{item.username}</div>)
                    : <Loader active inline inverted />
                }
              </Popup>
            )}
          </span>
        </CommentUI.Actions>
      </CommentUI.Content>
      {authorizedUser === user.id && (
        <Menu
          borderless
          size="mini"
          style={{ position: 'absolute', top: 0, right: 0, marginRight: 50, border: 'none' }}
        >
          <Dropdown
            item
            text="Actions"
          >
            <Dropdown.Menu>
              <Dropdown.Item onClick={handleEditOwnComment}>Update comment</Dropdown.Item>
              <Dropdown.Item onClick={handleDeleteOwnComment}>Delete comment</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Menu>
      )}
    </CommentUI>
  );
};
Comment.defaultProps = {
  commentLikesCount: 0,
  commentDislikesCount: 0
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onEditOwnComment: PropTypes.func.isRequired,
  commentLikesCount: PropTypes.number,
  commentDislikesCount: PropTypes.number,
  authorizedUser: PropTypes.string.isRequired,
  onCommentLike: PropTypes.func.isRequired,
  onDeleteOwnComment: PropTypes.func.isRequired,
  onDisplayCommentLikesList: PropTypes.func.isRequired
};

export default Comment;
