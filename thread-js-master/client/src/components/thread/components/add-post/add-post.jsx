import * as React from 'react';
import PropTypes from 'prop-types';
import { threadActionCreator } from 'src/store/actions';
import { useDispatch } from 'react-redux';
import { ButtonColor, ButtonType, IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment } from 'src/components/common/common';

import styles from './styles.module.scss';

const AddPost = ({ uploadImage }) => {
  const [body, setBody] = React.useState('');
  const [image, setImage] = React.useState(undefined);
  const [isUploading, setIsUploading] = React.useState(false);
  const dispatch = useDispatch();

  const handleAddPost = () => {
    if (!body) {
      return;
    }
    dispatch(threadActionCreator.createPost({ imageId: image?.imageId, body }));
    setBody('');
    setImage(undefined);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    const [file] = target.files;
    await uploadImage(file)
      .then(({ id: imageId, link: imageLink }) => {
        setImage({ imageId, imageLink });
      })
      .catch(() => {
        // dispatch(addTodoFailure(err.message));
        // Todo add errorHandler
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  return (
    <Segment>
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <div className={styles.btnWrapper}>
          <Button
            color="teal"
            isLoading={isUploading}
            isDisabled={isUploading}
            iconName={IconName.IMAGE}
          >
            <label className={styles.btnImgLabel}>
              Attach image
              <input
                name="image"
                type="file"
                onChange={handleUploadFile}
                hidden
              />
            </label>
          </Button>
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
            Post
          </Button>
        </div>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  uploadImage: PropTypes.func.isRequired
};

export default AddPost;
