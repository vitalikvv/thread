import * as React from 'react';
import PropTypes from 'prop-types';
import { threadActionCreator } from 'src/store/actions';
import { useDispatch } from 'react-redux';
import { ButtonColor, ButtonType } from 'src/common/enums/enums';
import { Button, Form, Segment } from 'src/components/common/common';

import styles from './styles.module.scss';
import { Modal } from '../../../common/common';
import { commentType } from '../../../../common/prop-types/comment';

const UpdateComment = ({ comment, close }) => {
  const [body, setBody] = React.useState(comment.body);
  const [openModal, setOpenModal] = React.useState(true);
  const dispatch = useDispatch();
  const handleUpdateComment = () => {
    if (!body) {
      return;
    }
    dispatch(threadActionCreator.updateComment({ body, id: comment.id, postId: comment.postId }));
    setBody('');
    close();
  };

  React.useEffect(() => {
    if (!openModal) {
      close();
    }
  }, [openModal]);
  return (
    <Modal
      open={openModal}
      onClose={() => setOpenModal(false)}
    >
      <Modal.Header className={styles.header}>
        <span>Update your comment</span>
      </Modal.Header>
      <Modal.Content>
        <Segment>
          <Form onSubmit={handleUpdateComment}>
            <Form.TextArea
              name="body"
              value={body}
              placeholder="What is the news?"
              onChange={ev => setBody(ev.target.value)}
            />
            <div className={styles.btnWrapper}>
              <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                Update
              </Button>
            </div>
          </Form>
        </Segment>
      </Modal.Content>
    </Modal>
  );
};

UpdateComment.propTypes = {
  comment: commentType.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdateComment;
