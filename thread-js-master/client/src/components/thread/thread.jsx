import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';
import UpdatePost from './components/update-post/update-post';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));

  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showPostsWithOwnLikes, setShowPostsWithOwnLikes] = React.useState(false);
  const [hideOwnPosts, setHideOwnPosts] = React.useState(false);
  const [showUpdateOwnPostComponent, setShowUpdateOwnPostComponent] = React.useState(undefined);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback((id, likeOrDislike) => (
    dispatch(threadActionCreator.likePost(id, likeOrDislike))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const onDisplayList = React.useCallback((id, listOfLikesOrDis) => (
    dispatch(threadActionCreator.displayListOfLikes(id, listOfLikesOrDis))
  ), [dispatch]);

  const handlePostUpdate = postPayload => {
    dispatch(threadActionCreator.updatePost(postPayload));
  };

  const handleDeletePost = postId => {
    dispatch(threadActionCreator.deletePost(postId));
  };

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const makeQueryFilter = (
    setShowMyPosts,
    setHideMyPosts,
    setShowPostsWithMyLikes,
    hideMyPosts,
    postsWithMyLikes,
    userID
  ) => {
    setShowOwnPosts(setShowMyPosts);
    setHideOwnPosts(setHideMyPosts);
    setShowPostsWithOwnLikes(setShowPostsWithMyLikes);
    postsFilter.hideMyPosts = hideMyPosts;
    postsFilter.likedByMe = postsWithMyLikes;
    postsFilter.userId = userID;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    makeQueryFilter(
      !showOwnPosts,
      false,
      false,
      undefined,
      undefined,
      showOwnPosts ? undefined : userId
    );
  };

  const toggleShowPostsLikedByMe = () => {
    makeQueryFilter(
      false,
      false,
      !showPostsWithOwnLikes,
      undefined,
      showPostsWithOwnLikes ? undefined : true,
      showPostsWithOwnLikes ? undefined : userId
    );
  };

  const toggleHideOwnPosts = () => {
    makeQueryFilter(
      false,
      !hideOwnPosts,
      false,
      hideOwnPosts ? undefined : true,
      undefined,
      hideOwnPosts ? undefined : userId
    );
  };

  const getMorePosts = () => {
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const handleEditOwnPost = post => setShowUpdateOwnPostComponent(post);

  const uploadImage = file => imageService.uploadImage(file);
  const updateImage = file => imageService.updateImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          style={{ marginLeft: 14 }}
          toggle
          label="Show posts liked by me"
          checked={showPostsWithOwnLikes}
          onChange={toggleShowPostsLikedByMe}
        />
        <Checkbox
          style={{ marginLeft: 14 }}
          toggle
          label="Hide own posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            authorizedUser={userId}
            onPostLike={handlePostLike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onEditOwnPost={handleEditOwnPost}
            onDeleteOwnPost={handleDeletePost}
            sharePost={sharePost}
            onDisplayList={onDisplayList}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
      {showUpdateOwnPostComponent && (
        <UpdatePost
          post={showUpdateOwnPostComponent}
          onPostUpdate={handlePostUpdate}
          uploadImage={updateImage}
          close={() => setShowUpdateOwnPostComponent(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
